import re
import json
import base64
import os

input_name = "README.tmpl.md"
output_name = "README.md"

pattern = " *<!-- jupyter:(.*):tag:(.*):(.*) -->"

lines = []
new_lines = []

def extract_cell_output(file_name, tag, output_type):
    
    if output_type not in {"image/png", "text/html"}:
        raise ArgumentError(f"I don't know what how to extract {output_type}")
    
    jupyter_file = None
    
    with open(file_name,  "r") as f:
        jupyter_file = json.load(f)
    
    cell = next(cell for cell in jupyter_file["cells"] if "tags" in cell["metadata"] and tag in cell["metadata"]["tags"])
    
    if output_type == "image/png":
        output = next(output["data"][output_type] for output in cell["outputs"] if output["output_type"]=="display_data")
    if output_type == "text/html":
        
        output = next(output["data"][output_type] for output in cell["outputs"] if output["output_type"]=="execute_result")
        
        try:
            output = output[0:output.index('<style scoped>\n')]+output[output.index('</style>\n')+1:-1]
        except ValueError:
            print('No style tag found')
        
    return output
    
extract_cell_output("sample.ipynb", "senoid", "image/png")

with open(input_name) as origin:
    
    for line in origin.readlines():
        match = re.match(pattern, line)
        if not match:
            new_lines.append(line)
            
        else:
            file_name = match.group(1)
            cell_tag = match.group(2)
            output_type = match.group(3)
            
            output = extract_cell_output(file_name, cell_tag, output_type)
            
            if output_type == "image/png":
                os.makedirs("images", exist_ok=True)
                
                with open(f"images/{cell_tag}.png", "wb") as f:
                    f.write(base64.b64decode(output))
                
                new_lines.append(f"![](images/{cell_tag}.png)")

            if output_type == "text/html":
                new_lines = new_lines + extract_cell_output(file_name, cell_tag, output_type)
            
            new_lines.append("\n")

with open(output_name, "w") as o_file:
    o_file.writelines(new_lines)
