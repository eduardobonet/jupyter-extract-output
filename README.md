# This README is generated by a script

What's cool about this? is that the image and the table that you 
see here actually come from a Jupyter Notebook cell. 

The interesting thing about this file is that it is extracting
an image an a table from a jupyter notebook, how cool is that?

So, let's see the image:

![](images/senoid.png)


Can we also do this with a table?

<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>x</th>
      <th>y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.000000</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.256457</td>
      <td>2.536546e-01</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.512913</td>
      <td>4.907176e-01</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.769370</td>
      <td>6.956826e-01</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.025826</td>
      <td>8.551428e-01</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1.282283</td>
      <td>9.586679e-01</td>
    </tr>
    <tr>
      <th>6</th>
      <td>1.538739</td>
      <td>9.994862e-01</td>
    </tr>
    <tr>
      <th>7</th>
      <td>1.795196</td>
      <td>9.749279e-01</td>
    </tr>
    <tr>
      <th>8</th>
      <td>2.051652</td>
      <td>8.865993e-01</td>
    </tr>
    <tr>
      <th>9</th>
      <td>2.308109</td>
      <td>7.402780e-01</td>
    </tr>
    <tr>
      <th>10</th>
      <td>2.564565</td>
      <td>5.455349e-01</td>
    </tr>
    <tr>
      <th>11</th>
      <td>2.821022</td>
      <td>3.151082e-01</td>
    </tr>
    <tr>
      <th>12</th>
      <td>3.077479</td>
      <td>6.407022e-02</td>
    </tr>
    <tr>
      <th>13</th>
      <td>3.333935</td>
      <td>-1.911586e-01</td>
    </tr>
    <tr>
      <th>14</th>
      <td>3.590392</td>
      <td>-4.338837e-01</td>
    </tr>
    <tr>
      <th>15</th>
      <td>3.846848</td>
      <td>-6.482284e-01</td>
    </tr>
    <tr>
      <th>16</th>
      <td>4.103305</td>
      <td>-8.201723e-01</td>
    </tr>
    <tr>
      <th>17</th>
      <td>4.359761</td>
      <td>-9.384684e-01</td>
    </tr>
    <tr>
      <th>18</th>
      <td>4.616218</td>
      <td>-9.953791e-01</td>
    </tr>
    <tr>
      <th>19</th>
      <td>4.872674</td>
      <td>-9.871818e-01</td>
    </tr>
    <tr>
      <th>20</th>
      <td>5.129131</td>
      <td>-9.144126e-01</td>
    </tr>
    <tr>
      <th>21</th>
      <td>5.385587</td>
      <td>-7.818315e-01</td>
    </tr>
    <tr>
      <th>22</th>
      <td>5.642044</td>
      <td>-5.981105e-01</td>
    </tr>
    <tr>
      <th>23</th>
      <td>5.898500</td>
      <td>-3.752670e-01</td>
    </tr>
    <tr>
      <th>24</th>
      <td>6.154957</td>
      <td>-1.278772e-01</td>
    </tr>
    <tr>
      <th>25</th>
      <td>6.411414</td>
      <td>1.278772e-01</td>
    </tr>
    <tr>
      <th>26</th>
      <td>6.667870</td>
      <td>3.752670e-01</td>
    </tr>
    <tr>
      <th>27</th>
      <td>6.924327</td>
      <td>5.981105e-01</td>
    </tr>
    <tr>
      <th>28</th>
      <td>7.180783</td>
      <td>7.818315e-01</td>
    </tr>
    <tr>
      <th>29</th>
      <td>7.437240</td>
      <td>9.144126e-01</td>
    </tr>
    <tr>
      <th>30</th>
      <td>7.693696</td>
      <td>9.871818e-01</td>
    </tr>
    <tr>
      <th>31</th>
      <td>7.950153</td>
      <td>9.953791e-01</td>
    </tr>
    <tr>
      <th>32</th>
      <td>8.206609</td>
      <td>9.384684e-01</td>
    </tr>
    <tr>
      <th>33</th>
      <td>8.463066</td>
      <td>8.201723e-01</td>
    </tr>
    <tr>
      <th>34</th>
      <td>8.719522</td>
      <td>6.482284e-01</td>
    </tr>
    <tr>
      <th>35</th>
      <td>8.975979</td>
      <td>4.338837e-01</td>
    </tr>
    <tr>
      <th>36</th>
      <td>9.232436</td>
      <td>1.911586e-01</td>
    </tr>
    <tr>
      <th>37</th>
      <td>9.488892</td>
      <td>-6.407022e-02</td>
    </tr>
    <tr>
      <th>38</th>
      <td>9.745349</td>
      <td>-3.151082e-01</td>
    </tr>
    <tr>
      <th>39</th>
      <td>10.001805</td>
      <td>-5.455349e-01</td>
    </tr>
    <tr>
      <th>40</th>
      <td>10.258262</td>
      <td>-7.402780e-01</td>
    </tr>
    <tr>
      <th>41</th>
      <td>10.514718</td>
      <td>-8.865993e-01</td>
    </tr>
    <tr>
      <th>42</th>
      <td>10.771175</td>
      <td>-9.749279e-01</td>
    </tr>
    <tr>
      <th>43</th>
      <td>11.027631</td>
      <td>-9.994862e-01</td>
    </tr>
    <tr>
      <th>44</th>
      <td>11.284088</td>
      <td>-9.586679e-01</td>
    </tr>
    <tr>
      <th>45</th>
      <td>11.540544</td>
      <td>-8.551428e-01</td>
    </tr>
    <tr>
      <th>46</th>
      <td>11.797001</td>
      <td>-6.956826e-01</td>
    </tr>
    <tr>
      <th>47</th>
      <td>12.053458</td>
      <td>-4.907176e-01</td>
    </tr>
    <tr>
      <th>48</th>
      <td>12.309914</td>
      <td>-2.536546e-01</td>
    </tr>
    <tr>
      <th>49</th>
      <td>12.566371</td>
      <td>-4.898587e-16</td>
    </tr>
  </tbody>
</table>



## How does it work?

Jupyter is a json file, and the tags are located for a cell are located in the `$.cells[0].metadata.tags`, 
so we can find a specific cell that we want to pull the results from. [I added a copy of the sample notebook 
and renamed to .json so you can check it as well](sample.json). The tricky part is how to extract the output we want:

- Images are on for outputs that are `output_data='display_data'`, and they are encoded as bas64
- Tables are HTML on outputs of type `output_data='execute_results'`, not entirely sure why the distinction

## Disclaimer


This is an example, not a library, but it could become a library.  I didn't care about good code, it's just 
meant to serve as an example to show that this is indeed possible. Some stuff I would still like to see:

- Render variables from the jupyter notebook
- Equations
- Why not widgets? they are HTML after all
- SVG images
